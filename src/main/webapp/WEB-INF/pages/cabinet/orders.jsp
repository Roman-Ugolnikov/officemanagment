<%--@elvariable id="orders" type="org.springframework.data.domain.Page<com.me.mvc.persistence.model.entity.BusinessOrder>"--%>
<%--@elvariable id="order" type="com.me.mvc.persistence.model.entity.BusinessOrder"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
    <%@include file="../pieces/header.jsp"%>
    <meta name="title" content="Заказы клиентов">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
    <%--main message on the page--%>
    <div class="jumbotron">
        <h1>Заказы клиентов</h1>
        <p>Заказы клиентов</p>
    </div>

    <%--content--%>
    <div class="bs-example" data-example-id="striped-table">

        <a href="${baseURL}/cabinet/orders/newOrder">
        <button type="button" class="btn btn-lg btn-success">Создать новый</button>
        </a>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>id</th>
                <th>created</th>
                <th>start time</th>
                <th>end time</th>
                <th>Status</th>
                <th>Room</th>
                <th>Staff</th>
                <th>client</th>
                <th>info</th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${not empty orders.content}">
                <c:forEach var="order" varStatus="status" items="${orders.content}">
                    <tr>
                        <td><a href="${baseURL}/cabinet/orders/${order.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><c:out value="${order.id}" /></a></td>
                        <td><c:out value="${order.creationTime}"/></td>
                        <td><c:out value="${order.startTime}" /></td>
                        <td><c:out value="${order.endTime}" /></td>
                        <td><c:out value="${order.orderStatus.title}" /></td>
                        <td><c:out value="${order.staff.name}" /></td>
                        <td><c:out value="${order.room.title}" /></td>
                        <td><c:out value="${order.client.name}" /></td>
                        <td><c:out value="${order.details}" /></td>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table>

        <office:renderPaginationForPage page="${orders}" />

        <c:if test="${empty orders.content}">
            <div class="alert alert-warning" role="alert">таблица пуста</div>
        </c:if>
    </div>
</div>
</body>
</html>