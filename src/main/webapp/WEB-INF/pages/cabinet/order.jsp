<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp" %>
<html>
<head>
  <%@include file="../pieces/header.jsp" %>
  <meta name="title" content="Заказы клиентов">
</head>
<div role="document">
  <%--main navigation panel--%>
  <jsp:include page="../pieces/mainnavbar.jsp"/>

  <div class="container theme-showcase" role="main">
    <%--main message on the page--%>
    <div class="jumbotron">
      <h1>Заказы клиентов</h1>
      <p>Заказы клиентов</p>
    </div>

      <%--breadcrumb--%>
      <ol class="breadcrumb">
        <li><a href="${baseURL}">Home</a></li>
        <li><a href="${baseURL}/cabinet/orders">All orders</a></li>
        <li class="active">Order</li>
      </ol>
    <%--content--%>
    <div class="panel panel-default">
      <div class="panel-body">
        <div>


          <form:form method="post" modelAttribute="order" action="${baseURL}/cabinet/orders" cssClass="form-horizontal">
            <div class="form-group"> id: <form:input path="id" readonly="true" cssClass="form-control"/></div>
            <div class="form-group">creationTime: <form:input path="creationTime" readonly="true" cssClass="form-control"/></div>
            <div class="form-group">startTime : <form:input path="startTime" cssClass="form-control"/></div>
            <div class="form-group">endTime: <form:input path="endTime" cssClass="form-control"/></div>

            <div class="form-group"> order status:
              <form:select path="orderStatus"  cssClass="form-control">
                <form:options items="${orderStatuses}" itemValue="id" itemLabel="title"/>
              </form:select>
            </div>

            <div class="form-group" > room:
              <form:select path="room"  cssClass="form-control">
                <form:options items="${rooms}" itemValue="id" itemLabel="title"/>
              </form:select>
            </div>

            <div class="form-group"> client:
              <form:select path="client" cssClass="form-control">
                <form:options items="${clients}" itemValue="id" itemLabel="name"/>
              </form:select>
            </div>

            <div class="form-group"> staff:
              <form:select path="staff" cssClass="form-control">
                <form:options items="${staffs}" itemValue="id" itemLabel="name"/>
              </form:select>
            </div>


            <div class="form-group">info: <form:textarea path="details" cssClass="form-control" /></div>
            <p>
              <input type="submit" value="Сохранить"/>
            </p>
            <c:if test="${not empty result.allErrors}">
              <div class="alert alert-danger" role="alert">
                <b><c:out value="${result.allErrors[0].field}"/>:</b>
                <c:out value="${result.allErrors[0].code}"/>
              </div>
            </c:if>
          </form:form>
        </div>
      </div>

    </div>
  </div>
</div>
</body>
</html>