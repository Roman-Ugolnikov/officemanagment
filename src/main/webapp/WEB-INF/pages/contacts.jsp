<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="pieces/header.jsp"%>
  <meta name="title" content="<fmt:message key="index.page.title"/>">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1><fmt:message key="contacts.page.title"/></h1>
  <p><fmt:message key="contacts.page.title.description"/>Here we will see title description</p>
</div>

<%--content--%>
<div class="page-header">
  <h1>Header</h1>
</div>
<p>
<div class="panel panel-default">
    <div class="panel-body">
        Skype: ugolnikovroman
     </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        phone: +380668801524
     </div>
</div>

</p>

</div>
</body>
</html>