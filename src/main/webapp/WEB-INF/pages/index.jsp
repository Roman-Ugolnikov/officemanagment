<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="taglibs/taglibs.jsp" %>
<html>
<head>
  <%@include file="pieces/header.jsp" %>
  <meta name="title" content="<fmt:message key="index.page.title"/>">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="pieces/mainnavbar.jsp"/>

<div class="container theme-showcase" role="main">
  <%--main message on the page--%>
  <div class="jumbotron">
    <h1><fmt:message key="index.page.title"/></h1>
    <p><fmt:message key="index.page.title.description"/></p>
  </div>

  <%--content--%>
  <div class="page-header">
    <h1>Header</h1>
  </div>
  <p>Добро пожаловать.
  </p>

  <p>Тестовый сайт-черновик.</p>
  <center><img src="${baseURL}/resources/images/welcome.gif"/></center>

  <br />
  <center>
    <a href="${baseURL}/cabinet/orders/newOrder">
      <button type="button" class="btn btn-lg btn-success">Создать заказ на обслуживание</button>
    </a>
  </center>

</div>
</body>
</html>