<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<!-- Bootstrap core CSS -->
<link href="${baseURL}/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="${baseURL}/resources/css/bootstrap-theme.min.css" rel="stylesheet">
<!--<script src="/resources/js/jquery-1.11.3.min.js"></script>-->
 <!--jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${baseURL}/resources/js/g.jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${baseURL}/resources/js/bootstrap.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="${baseURL}/resources/css/optional/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="${baseURL}/resources/css/optional/bootstrap-theme.min.css">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="author" content="u-roma@mail.ru">


