<%@include file="../taglibs/taglibs.jsp"%>
<%@ page import="org.springframework.context.i18n.LocaleContextHolder" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"  title="тут будет имя клиники с базы данных System Properties">Clinic name</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <%--flags--%>
        <li class="dropdown">
        <c:set var="language" value="<%= LocaleContextHolder.getLocale().getLanguage() %>" />
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img width="24" height="16" src="${baseURL}/resources/images/flags/${language}.png" /><span class="caret"></span></a>
            <c:choose>
              <c:when test="${language eq 'ru'}">
                <ul class="dropdown-menu" role="menu">
                  <li><a href="?lang=en"><img width="24" height="16" src="${baseURL}/resources/images/flags/en.png" alt="en"></a></li>
                  <li><a href="?lang=ua"><img width="24" height="16" src="${baseURL}/resources/images/flags/ua.png"></a></li>
                </ul>
              </c:when>
              <c:when test="${language eq 'ua'}">
                <ul class="dropdown-menu" role="menu">
                  <li><a href="?lang=en"><img width="24" height="16" src="${baseURL}/resources/images/flags/en.png"></a></li>
                  <li><a href="?lang=ru"><img width="24" height="16" src="${baseURL}/resources/images/flags/ru.png"></a></li>
                </ul>
              </c:when>
              <c:otherwise>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="?lang=ua"><img width="24" height="16" src="${baseURL}/resources/images/flags/ua.png"></a></li>
                  <li><a href="?lang=ru"><img width="24" height="16" src="${baseURL}/resources/images/flags/ru.png"></a></li>
                </ul>
              </c:otherwise>
            </c:choose>
        </li>
        <%--end flags--%>
        <%--<li><a  <c:if test="${1 <= 1}"> class="active"</c:if> href="${baseURL}"><fmt:message key="main.menu.home"/></a></li>--%>
        <li><a href="${baseURL}"><fmt:message key="main.menu.home"/></a></li>
        <li><a href="${baseURL}/about"><fmt:message key="main.menu.about"/></a></li>
        <li><a href="${baseURL}/contacts"><fmt:message key="main.menu.contanct"/></a></li>
        <li><a href="${baseURL}/tables"><fmt:message key="main.menu.settings"/></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><fmt:message key="cabinet" /> <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${baseURL}/cabinet/orders"><fmt:message key="orders" /> </a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li class="dropdown-header">Nav header</li>
            <li><a href="#">Separated link</a></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <div class="navbar-form navbar-right badge">

        <%= SecurityContextHolder.getContext().getAuthentication().getName() %>
        <a href="${baseURL}/logout" title="log out">
          <img src="${baseURL}/resources/images/logout.png" alt="log out" width="43" />
        </a>

      </div>
    </div>
  </div>
</nav>
