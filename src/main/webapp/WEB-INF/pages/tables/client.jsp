<%--@elvariable id="client" type="com.me.mvc.persistence.model.entity.Client"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="Клиенты">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1>Клиенты</h1>
  <p>Заполните все поля</p>
</div>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">

    <form:form method="post"  modelAttribute="client" action="${baseURL}/tables/clients">
    <div>
      <div class="input-group">
        <span class="input-group-addon" id="ids">id</span>
        <form:input path="id" readonly="true" cssClass="form-control"/>
        <%--<input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">--%>
      </div>
      <br/>
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">name</span>
        <form:input path="name" cssClass="form-control"/>
      </div>
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1address">address</span>
        <form:input path="address" cssClass="form-control"/>
      </div>
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1birthday">birthday</span>
        <form:input path="birthday" cssClass="form-control"/>
      </div>
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1birthday">email</span>
        <form:input path="email" cssClass="form-control"/>
      </div>
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1birthday">contacts</span>
        <form:input path="contacts" cssClass="form-control"/>
      </div>
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1birthday">additionalInfo</span>
        <form:input path="additionalInfo" cssClass="form-control"/>
      </div>
      <!-- other staff here?-->

      <br/>
      <div class="input-group">
        <input type="submit" value="Сохранить"/>
      </div>
    </div>
  </form:form>

</div>
</div>
</body>
</html>