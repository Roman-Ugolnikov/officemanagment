<%--@elvariable id="clientGroups" type="org.springframework.data.domain.Page<com.me.mvc.persistence.model.entity.Client>"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="Клиенты">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1>Клиенты</h1>
  <p>Если вы собираетесь группировать клиенов, исползуйте группы</p>
</div>

  <%--breadcrumb--%>
  <ol class="breadcrumb">
    <li><a href="${baseURL}">Home</a></li>
    <li  class="active">Clients</li>
    <li><a href="${baseURL}/tables/clientGroups">Groups</a></li>
    <li> <a href="${baseURL}/tables/clientInGroup">Client in the Group</a></li>
  </ol>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">


    <table class="table table-striped table-hover">
    <thead>
        <tr>
          <th>id</th>
          <th>name</th>
          <th>contacts</th>
          <th>birthday</th>
        </tr>
    </thead>
    <tbody>
    <c:if test="${not empty clients.content}">
       <c:forEach var="client" varStatus="status" items="${clients.content}">
       <tr>
        <td><c:out value="${client.id}" /></td>
        <td><c:out value="${client.name}"/></td>
        <td><c:out value="${client.contacts}" /></td>
        <td><a href="${baseURL}/tables/clients/${client.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
       </tr>
       </c:forEach>
    </c:if>
    </tbody>
    </table>
        <a href="${baseURL}/tables/clients/newClient" >Создать </a>

   <office:renderPaginationForPage page="${clients}" />

  <c:if test="${empty clients.content}">
      <div class="alert alert-warning" role="alert">таблица пуста</div>
   </c:if>
</div>
</div>
</body>
</html>