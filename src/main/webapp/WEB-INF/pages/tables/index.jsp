<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="<fmt:message key="index.page.title"/>">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1><fmt:message key="settings.page.title"/></h1>
  <p><fmt:message key="settings.page.title.description"/></p>
</div>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">
  <table class="table table-striped table-hover">
    <thead>
    <tr>
      <th>#</th>
      <th>Назавание</th>
      <th>Кратное описание</th>
      <th>Количестов записей</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <th scope="row">1</th>
      <td><a href="${baseURL}/tables/properties">Свойства системы</a></td>
      <td>Свойства системы которые могут быть изменены пользователем</td>
      <td>${systemPropertyCapacity}</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td><a href="${baseURL}/tables/staff">Cотрудники</a></td>
      <td>Сотрудники клинники, зарегестрированняе в системе</td>
      <td>//todo</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td><a href="${baseURL}/tables/clientGroups">Круги клиентов</a></td>
      <td>Группы, по которым мы группируем клиентов.</td>
      <td>//todo</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td><a href="${baseURL}/tables/clients">Киенты</a></td>
      <td>Клиенты клиннники</td>
      <td>//todo</td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td><a href="${baseURL}/tables/clientInGroup">Клиенты в Группе</a></td>
      <td>Обьеденения в группы.</td>
      <td>//todo</td>
    </tr>
    <tr>
      <th scope="row">5</th>
      <td><a href="${baseURL}/tables/orderStatuses">Статусы заказов</a></td>
      <td>Список заказов которые может иметь заказ</td>
      <td>//todo</td>
    </tr>
    </tbody>
  </table>
</div>



</div>
</body>
</html>