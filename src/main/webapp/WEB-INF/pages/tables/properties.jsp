<%--@elvariable id="systemProperties" type="org.springframework.data.domain.Page<com.me.mvc.persistence.model.entity.SystemProperty>"--%>
<%--@elvariable id="newProperty" type="com.me.mvc.persistence.model.entity.SystemProperty"--%>
<%--@elvariable id="result" type="org.springframework.validation.BindingResult"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="<fmt:message key="index.page.title"/>">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1>Свойства системы</h1>
  <p>Настраиваемые свойства системы</p>
</div>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">

  <c:if test="${not empty systemProperties.content}">
    <table class="table table-striped table-hover">
    <thead>
        <tr>
          <th>id</th>
          <th>key</th>
          <th>value</th>
          <th>edit</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <form:form method="post"  modelAttribute="newProperty" action="${baseURL}/tables/properties">
            <td><form:input path="id" readonly="true"/></td>
            <td><form:input path="key"/></td>
            <td><form:input path="value"/></td>
            <td><input type="submit" value="Сохранить"/></td>
            <c:if test="${not empty result.allErrors}">
                <div class="alert alert-danger" role="alert">
                    <form:errors path="id"/>
                    <form:errors path="key"/>
                    <form:errors path="value"/>
                    <b><c:out value="${result.allErrors[0].field}" />:</b>
                    <c:out value="${result.allErrors[0].code}" />
                </div>
            </c:if>
        </form:form>
    </tr>
       <c:forEach var="clientInGroup" varStatus="status" items="${systemProperties.content}">
       <tr>
        <td><c:out value="${clientInGroup.id}" /></td>
        <td><c:out value="${clientInGroup.key}"/></td>
        <td><c:out value="${clientInGroup.value}"/></td>
        <td><a href="${baseURL}/tables/properties/${clientInGroup.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
       </tr>
       </c:forEach>

    </tbody>
    </table>

   <office:renderPaginationForPage page="${systemProperties}" />

  </c:if>
  <c:if test="${empty systemProperties.content}">
      <div class="alert alert-warning" role="alert">таблица пуста</div>
   </c:if>
</div>
</div>
</body>
</html>