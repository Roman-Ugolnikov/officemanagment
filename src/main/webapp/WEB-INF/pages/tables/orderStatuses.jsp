<%--@elvariable id="orderStatuses" type="org.springframework.data.domain.Page<com.me.mvc.persistence.model.entity.OrderStatus>"--%>
<%--@elvariable id="newOrderStatus" type="com.me.mvc.persistence.model.entity.OrderStatus"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="Статусы Заказов">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1>Статусы Заказов</h1>
  <p>Каждый заказ может находится разных статусах.</p>
</div>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">


    <table class="table table-striped table-hover">
    <thead>
        <tr>
          <th>id</th>
          <th>title</th>
          <th>description</th>
          <th>edit</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <form:form method="post"  modelAttribute="newOrderStatus" action="${baseURL}/tables/orderStatuses">
            <td><form:input path="id" readonly="true"/></td>
            <td><form:input path="title"/></td>
            <td><form:textarea path="description"/></td>
            <td><input type="submit" value="Сохранить"/></td>
            <c:if test="${not empty result.allErrors}">
                <div class="alert alert-danger" role="alert">
                    <form:errors path="id"/>
                    <form:errors path="title"/>
                    <form:errors path="description"/>
                    <b><c:out value="${result.allErrors[0].field}" />:</b>
                    <c:out value="${result.allErrors[0].code}" />
                </div>
            </c:if>
        </form:form>
    </tr>
    <c:if test="${not empty orderStatuses.content}">
       <c:forEach var="orderStatus" varStatus="status" items="${orderStatuses.content}">
       <tr>
        <td><c:out value="${orderStatus.id}" /></td>
        <td><c:out value="${orderStatus.title}"/></td>
        <td><c:out value="${orderStatus.description}" /></td>
        <td><a href="${baseURL}/tables/orderStatuses/${orderStatus.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
       </tr>
       </c:forEach>
    </c:if>
    </tbody>
    </table>

   <office:renderPaginationForPage page="${orderStatuses}" />

  <c:if test="${empty orderStatuses.content}">
      <div class="alert alert-warning" role="alert">таблица пуста</div>
   </c:if>
</div>
</div>
</body>
</html>