<%--@elvariable id="clientGroups" type="org.springframework.data.domain.Page<com.me.mvc.persistence.model.entity.ClientGroup>"--%>
<%--@elvariable id="newClientGroup" type="com.me.mvc.persistence.model.entity.ClientGroup"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="Группы клиентов">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1>Группы клиентов</h1>
  <p>Если вы собираетесь группировать клиенов, исползуйте группы</p>
</div>

  <%--breadcrumb--%>
  <ol class="breadcrumb">
    <li><a href="${baseURL}">Home</a></li>
    <li><a href="${baseURL}/tables/clients">Clients</a></li>
    <li class="active">Groups</li>
    <li> <a href="${baseURL}/tables/clientInGroup">Client in the Group</a></li>
  </ol>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">


    <table class="table table-striped table-hover">
    <thead>
        <tr>
          <th>id</th>
          <th>title</th>
          <th>additional info</th>
          <th>edit</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <form:form method="post"  modelAttribute="newClientGroup" action="${baseURL}/tables/clientGroups">
            <td><form:input path="id"  readonly="true" cssClass="form-control"/></td>
            <td><form:input path="title"/></td>
            <td><form:textarea path="description"/></td>
            <td><input type="submit" value="Сохранить"/></td>
            <c:if test="${not empty result.allErrors}">
                <div class="alert alert-danger" role="alert">
                    <form:errors path="id"/>
                    <form:errors path="title"/>
                    <form:errors path="description"/>
                    <b><c:out value="${result.allErrors[0].field}" />:</b>
                    <c:out value="${result.allErrors[0].code}" />
                </div>
            </c:if>
        </form:form>
    </tr>
    <c:if test="${not empty clientGroups.content}">
       <c:forEach var="clientInGroup" varStatus="status" items="${clientGroups.content}">
       <tr>
        <td><c:out value="${clientInGroup.id}" /></td>
        <td><c:out value="${clientInGroup.title}"/></td>
        <td><c:out value="${clientInGroup.description}" /></td>
        <td><a href="${baseURL}/tables/clientGroups/${clientInGroup.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
       </tr>
       </c:forEach>
    </c:if>
    </tbody>
    </table>

   <office:renderPaginationForPage page="${clientGroups}" />

  <c:if test="${empty clientGroups.content}">
      <div class="alert alert-warning" role="alert">таблица пуста</div>
   </c:if>
</div>
</div>
</body>
</html>