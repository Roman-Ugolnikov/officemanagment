<%--@elvariable id="rooms" type="org.springframework.data.domain.Page<com.me.mvc.persistence.model.entity.Room>"--%>
<%--@elvariable id="newRoom" type="com.me.mvc.persistence.model.entity.Room"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="Кобинеты">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1>Кабинеты</h1>
  <p>Кабинеты которые доступны для клинники</p>
</div>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">


    <table class="table table-striped table-hover">
    <thead>
        <tr>
          <th>id</th>
          <th>title</th>
          <th>description</th>
          <th>edit</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <form:form method="post"  modelAttribute="newRoom" action="${baseURL}/tables/rooms">
            <td><form:input path="id" readonly="true"/></td>
            <td><form:input path="title"/></td>
            <td><form:textarea path="description"/></td>
            <td><input type="submit" value="Сохранить"/></td>
            <c:if test="${not empty result.allErrors}">
                <div class="alert alert-danger" role="alert">
                    <form:errors path="id"/>
                    <form:errors path="title"/>
                    <form:errors path="description"/>
                    <b><c:out value="${result.allErrors[0].field}" />:</b>
                    <c:out value="${result.allErrors[0].code}" />
                </div>
            </c:if>
        </form:form>
    </tr>
    <c:if test="${not empty rooms.content}">
       <c:forEach var="room" varStatus="status" items="${rooms.content}">
       <tr>
        <td><c:out value="${room.id}" /></td>
        <td><c:out value="${room.title}"/></td>
        <td><c:out value="${room.description}" /></td>
        <td><a href="${baseURL}/tables/rooms/${room.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
       </tr>
       </c:forEach>
    </c:if>
    </tbody>
    </table>

   <office:renderPaginationForPage page="${rooms}" />

  <c:if test="${empty rooms.content}">
      <div class="alert alert-warning" role="alert">таблица пуста</div>
   </c:if>
</div>
</div>
</body>
</html>