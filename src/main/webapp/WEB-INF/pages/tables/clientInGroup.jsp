<%--@elvariable id="clientGroups" type="org.springframework.data.domain.Page<com.me.mvc.persistence.model.entity.ClientGroup>"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../taglibs/taglibs.jsp"%>
<html>
<head>
	<%@include file="../pieces/header.jsp"%>
  <meta name="title" content="Группы клиентов">
</head>
<body role="document">
<%--main navigation panel--%>
<jsp:include page="../pieces/mainnavbar.jsp" />

<div class="container theme-showcase" role="main">
<%--main message on the page--%>
<div class="jumbotron">
  <h1>Группы клиентов и клиенты в них</h1>
  <p>Если вы собираетесь группировать клиенов, исползуйте группы</p>
</div>

  <%--breadcrumb--%>
  <ol class="breadcrumb">
    <li><a href="${baseURL}">Home</a></li>
    <li><a href="${baseURL}/tables/clients">Clients</a></li>
    <li><a href="${baseURL}/tables/clientGroups">Groups</a></li>
    <li class="active">Client in the Group</li>
  </ol>

<%--content--%>
<div class="bs-example" data-example-id="striped-table">


    <table class="table table-striped table-hover">
    <thead>
        <tr>
          <th>id</th>
          <th>Client</th>
          <th>Group</th>
          <th>edit</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <form:form method="post"  modelAttribute="newClientInGroup" action="${baseURL}/tables/clientInGroup">
            <td><form:input path="id" readonly="true" cssClass="form-control"/></td>
            <td>
              <div class="form-group">
                <form:select path="client" cssClass="form-control">
                  <form:options items="${clients}" itemValue="id" itemLabel="name"/>
                </form:select>
              </div>
            </td>
          <td>
            <div class="form-group">
              <form:select path="clientGroup" cssClass="form-control">
                <form:options items="${clientGroups}" itemValue="id" itemLabel="title"/>
              </form:select>
            </div>
          </td>
            <td><input type="submit" value="Сохранить"/></td>
            <c:if test="${not empty result.allErrors}">
                <div class="alert alert-danger" role="alert">
                    <form:errors path="id"/>
                    <form:errors path="client"/>
                    <form:errors path="clientGroup"/>
                    <b><c:out value="${result.allErrors[0].field}" />:</b>
                    <c:out value="${result.allErrors[0].code}" />
                </div>
            </c:if>
        </form:form>
    </tr>
    <c:if test="${not empty clientInGroupPage.content}">
       <c:forEach var="clientInGroup" varStatus="status" items="${clientInGroupPage.content}">
       <tr>
        <td><c:out value="${clientInGroup.id}" /></td>
        <td><c:out value="${clientInGroup.client.name}"/></td>
        <td><c:out value="${clientInGroup.clientGroup.title}" /></td>
        <td><a href="${baseURL}/tables/clientInGroup/${clientInGroup.id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
       </tr>
       </c:forEach>
    </c:if>
    </tbody>
    </table>

   <office:renderPaginationForPage page="${clientInGroupPage}" />

  <c:if test="${empty clientInGroupPage.content}">
      <div class="alert alert-warning" role="alert">таблица пуста</div>
   </c:if>
</div>
</div>
</body>
</html>