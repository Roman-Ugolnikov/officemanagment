<%@include file="taglibs/taglibs.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <%@include file="pieces/header.jsp" %>

</head>

<body>

<div class="container">
  <div class="row">
    <div class="col-md-offset-3 col-md-5">
      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="j_username" class="sr-only">Email address</label>
        <input type="text" id="j_username" name="j_username" class="form-control" placeholder="Email address" required autofocus>
        <label for="j_password" class="sr-only">Password</label>
        <input type="password" id="j_password" name="j_password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me" name="rememnerme"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
    </div>
  </div>

</div> <!-- /container -->


</body>
</html>