package com.me.mvc.controller.tables;

import com.me.mvc.persistence.model.entity.SystemProperty;
import com.me.mvc.persistence.model.validator.SystemPropertyValidator;
import com.me.mvc.service.SystemPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by roman on 24.01.16.
 */
@Controller
@RequestMapping("/tables/properties")
public class SystemProperties {

    @Autowired
    private SystemPropertyService systemPropertyService;

    @Autowired
    private SystemPropertyValidator systemPropertyValidator;


    @RequestMapping(method = RequestMethod.GET)
    public String properties(ModelMap model,  Pageable pageable) {

        model.put("newProperty", new SystemProperty());
        model.addAttribute("systemProperties", (Page) systemPropertyService.getPage(pageable));
        return "tables/properties";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String propertyPost(ModelMap model,
                               @Valid @ModelAttribute("newProperty") SystemProperty systemProperty,
                               BindingResult result, Pageable pageable) {

        systemPropertyValidator.validate(systemProperty, result);
        model.put("result", result);
        model.addAttribute("systemProperties", (Page) systemPropertyService.getPage(pageable));

        if (result.hasErrors()) {
            model.put("newProperty", systemProperty);
        } else {
            systemPropertyService.saveEntity(systemProperty);
            model.put("newProperty", new SystemProperty());
        }
        return "tables/properties";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{propertyId}")
    public String propertyGet(ModelMap model, @PathVariable int propertyId, Pageable pageable) {

        model.addAttribute("newProperty", systemPropertyService.getEntity(propertyId));
        model.addAttribute("systemProperties", (Page) systemPropertyService.getPage(pageable));
        return "tables/properties";
    }
}
