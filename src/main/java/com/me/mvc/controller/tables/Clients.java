package com.me.mvc.controller.tables;

import com.me.mvc.persistence.model.entity.Client;
import com.me.mvc.service.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by roman on 24.01.16.
 */
@Controller
@RequestMapping("/tables/clients")
public class Clients {

    @Autowired
    private ClientService clientService;

    @RequestMapping(method = RequestMethod.GET)
    public String properties(ModelMap model,  Pageable pageable) {

        model.addAttribute("clients", (Page) clientService.getPage(pageable));
        return "tables/clients";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String propertyPost(ModelMap model,
                               @ModelAttribute("client") Client client,
                               Pageable pageable) {

        clientService.saveEntity(client);
        model.put("client", new Client());
        model.addAttribute("clients", (Page) clientService.getPage(pageable));
        return "tables/clients";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{clientId}")
    public String propertyGet(ModelMap model, @PathVariable int clientId, Pageable pageable) {

        Client entity = clientService.getEntity(clientId);
        if(entity != null) {
            model.addAttribute("client", entity);
            return "tables/client";
        } else {
            return "tables/clients";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/newClient")
    public String propertyCreate(ModelMap model, Pageable pageable) {

        model.addAttribute("client", new Client());
        return "tables/client";
    }
}
