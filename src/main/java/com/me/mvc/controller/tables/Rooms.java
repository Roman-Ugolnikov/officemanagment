package com.me.mvc.controller.tables;

import com.me.mvc.persistence.model.entity.Room;
import com.me.mvc.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by roman on 24.01.16.
 */
@Controller
@RequestMapping("/tables/rooms")
public class Rooms {

    @Autowired
    private RoomService roomService;

    @RequestMapping(method = RequestMethod.GET)
    public String properties(ModelMap model,  Pageable pageable) {

        model.put("newRoom", new Room());
        model.addAttribute("rooms", (Page) roomService.getPage(pageable));
        return "tables/rooms";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String propertyPost(ModelMap model,
                               @ModelAttribute("newRoom") Room room,
                               Pageable pageable) {

        roomService.saveEntity(room);
        model.put("newRoom", new Room());
        model.addAttribute("rooms", (Page) roomService.getPage(pageable));
        return "tables/rooms";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{roomId}")
    public String propertyGet(ModelMap model, @PathVariable int roomId, Pageable pageable) {

        model.addAttribute("newRoom", roomService.getEntity(roomId));
        model.addAttribute("rooms", (Page) roomService.getPage(pageable));
        return "tables/rooms";
    }
}
