package com.me.mvc.controller;

import java.text.MessageFormat;
import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.me.mvc.service.ClientService;
import com.me.mvc.service.LogService;
import com.me.mvc.service.SystemPropertyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class Home {

    private final Logger logger = LoggerFactory.getLogger(Home.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private SystemPropertyService systemPropertyService;

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hello world! root");
        clientService.getPage(1);
        return "index";
    }

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String printWelcome1(ModelMap model, HttpServletRequest request) {

        model.addAttribute("message", "Hello world! index");
        return "index";
    }

    @RequestMapping(value = "contacts", method = RequestMethod.GET)
    public String contacts(ModelMap model, HttpServletRequest request) {

        return "contacts";
    }

    @RequestMapping(value = "about", method = RequestMethod.GET)
    public String about(ModelMap model, HttpServletRequest request) {
        return "about";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String logout(ModelMap model, HttpServletRequest request) {
        return "login";
    }

}