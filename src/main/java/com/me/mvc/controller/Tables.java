package com.me.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Roman Uholnikov
 */
@Controller
@RequestMapping("/tables")
public class Tables {

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap model) {
        return "tables/index";
    }

}
