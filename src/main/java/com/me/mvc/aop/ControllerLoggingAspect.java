package com.me.mvc.aop;

import com.me.mvc.service.LogService;
import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import java.util.Arrays;

/**
 * @author Roman Uholnikov
 */
public class ControllerLoggingAspect {

    @Autowired
    @Qualifier("logService")
    private LogService logService;

    public void beforeGet(JoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        for (Object o : args) {
            if (o instanceof SecurityContextHolderAwareRequestWrapper){
                logService.logSystemInfo("Some one entered the "+ joinPoint.getTarget() + ": " + o);
            }
        }
    }

}
