package com.me.mvc.persistence.repository;

import java.util.List;

import com.me.mvc.persistence.model.entity.SystemProperty;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Roman Uholnikov
 */
@Repository
public interface SystemPropertyRepository extends JpaRepository<SystemProperty, Integer> {

    List<SystemProperty> findByKeyLike(String key);

}
