package com.me.mvc.persistence.repository;

import com.me.mvc.persistence.model.entity.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Roman Uholnikov
 */
@Repository
public interface OrderStatusRepository extends JpaRepository<OrderStatus, Integer> {
}
