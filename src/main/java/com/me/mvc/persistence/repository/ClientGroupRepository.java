package com.me.mvc.persistence.repository;

import com.me.mvc.persistence.model.entity.ClientGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Roman Uholnikov
 */
@Repository
public interface ClientGroupRepository extends JpaRepository<ClientGroup, Integer> {


    /**
     * Find persons like first name.
     */
    public Page<ClientGroup> findByTitleLike(String title, Pageable pageable);

    /**
     * Find persons by last name.
     */
    public Page<ClientGroup> findByDescription(String description, Pageable pageable);


}
