package com.me.mvc.persistence.model.entity;

import java.text.MessageFormat;

import javax.persistence.Entity;

/**
 * @author Roman Uholnikov
 */
@Entity
public class Staff extends AbstractPerson {

    private String speciality;

    public Staff() {
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(final String speciality) {
        this.speciality = speciality;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} (1)", getName(), getSpeciality());
    }
}
