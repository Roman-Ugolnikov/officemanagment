package com.me.mvc.persistence.model.entity;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * Business and system logs stored in DB.
 *
 * @author Roman Uholnikov
 */
@Entity
public class Log {

    public static int SYSTEM_TYPE = 0;
    public static int BUSINESS_TYPE = 1;

    public static int DEBUG_LEVEL = 40;
    public static int INFO_LEVEL = 30;
    public static int WARN_LEVEL = 20;
    public static int ERROR_LEVEL = 10;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int type;

    private int level;

    private LocalDateTime time;

    @Type(type = "text")
    private String message;

    public Log() {
    }

    public Log(int type, LocalDateTime time, String message) {
        this.type = type;
        this.time = time;
        this.message = message;
    }

    public Log(final int type, final String message) {
        this.type = type;
        this.level = Log.INFO_LEVEL;
        this.message = message;
        this.time = LocalDateTime.now();
    }

    public Log(final int type, final int level , final String message) {
        this.type = type;
        this.level = level;
        this.message = message;
        this.time = LocalDateTime.now();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Log log = (Log) o;

        if (id != log.id) {
            return false;
        }
        if (type != log.type) {
            return false;
        }
        if (!message.equals(log.message)) {
            return false;
        }
        if (!time.equals(log.time)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + type;
        result = 31 * result + time.hashCode();
        result = 31 * result + message.hashCode();
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(final int type) {
        this.type = type;
    }

    public void setTime(final LocalDateTime time) {
        this.time = time;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(final int level) {
        this.level = level;
    }
}
