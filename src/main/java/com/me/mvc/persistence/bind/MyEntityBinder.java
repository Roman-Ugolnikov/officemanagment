package com.me.mvc.persistence.bind;

import java.beans.PropertyEditorSupport;

import com.me.mvc.service.AbstractEntityService;

/**
 * @author Roman Uholnikov
 */
public class MyEntityBinder extends PropertyEditorSupport {

    private final AbstractEntityService entityService;

    public MyEntityBinder(final AbstractEntityService entityService) {
        super();
        this.entityService = entityService;
    }

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        this.setValue(entityService.getEntity(Integer.valueOf(text)));
    }

}
