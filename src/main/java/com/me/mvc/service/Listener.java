package com.me.mvc.service;

import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * @author Roman Uholnikov
 */
public class Listener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        System.out.println("---------------------------------------");
        System.out.println("read: "+ message);
        System.out.println("---------------------------------------");
    }
}
