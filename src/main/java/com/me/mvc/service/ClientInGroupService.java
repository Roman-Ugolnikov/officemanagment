package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.ClientInGroup;
import com.me.mvc.persistence.repository.ClientInGroupRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class ClientInGroupService extends AbstractEntityService<ClientInGroupRepository, ClientInGroup> {


}
