package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.OrderStatus;
import com.me.mvc.persistence.repository.OrderStatusRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class OrderStatusService extends AbstractEntityService<OrderStatusRepository, OrderStatus> {

}
