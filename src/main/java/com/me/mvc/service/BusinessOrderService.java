package com.me.mvc.service;

import com.me.mvc.persistence.model.entity.BusinessOrder;
import com.me.mvc.persistence.model.entity.Client;
import com.me.mvc.persistence.model.entity.ClientGroup;
import com.me.mvc.persistence.repository.BusinessOrderRepository;
import com.me.mvc.persistence.repository.ClientGroupRepository;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Roman Uholnikov
 */
@Service
@Transactional
public class BusinessOrderService extends AbstractEntityService<BusinessOrderRepository, BusinessOrder> {


}
