package com.me.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Abstract entity to store in ORM
 * @param <R> repository that is used for deal with entity
 * @param <I> Item class
 */
public abstract class AbstractEntityService <R extends JpaRepository, I> {

    @Autowired
    protected R repository;

    public static int ITEMS_PER_PAGE = 20;

    public AbstractEntityService() {
    }


    public AbstractEntityService(final R repository) {
        this.repository = repository;
    }

    /**
     * Returns a new object which specifies the the wanted result pageNumber.
     * @param pageIndex The index of the wanted result pageNumber
     * @return
     */
    public Pageable constructPageSpecification(int pageIndex) {
        Pageable pageSpecification = new PageRequest(pageIndex, ITEMS_PER_PAGE);
        return pageSpecification;
    }

    public Pageable constructPageSpecification(int pageIndex, int size) {
        Pageable pageSpecification = new PageRequest(pageIndex, size);
        return pageSpecification;
    }


    public  I saveEntity(I item) {
        I savedItem = (I) repository.save(item);
        return savedItem;
    }

    public  I getEntity(Integer id) {
        I item = (I) repository.findOne(id);
        return item;
    }

    public void deleteEntity(I entity) {
        repository.delete(entity);
    }

    public void deleteEntityById(int id) {
        repository.delete(id);
    }


    public I editEntity(I item) {
        return (I) repository.save(item);
    }

    public Page<I> getPage(int pageNumber) {
        return repository.findAll(constructPageSpecification(pageNumber));
    }

    public Page<I> getPage(int pageNumber, int size) {
        return repository.findAll(constructPageSpecification(pageNumber, size));
    }

    public Page<I> getPage(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public List<I> getAll() {
        return repository.findAll();
    }

    public long getItemsCount() {
        return repository.count();
    }

    public R getRepository() {
        return repository;
    }

    public void setRepository(final R repository) {
        this.repository = repository;
    }
}
